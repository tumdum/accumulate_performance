#include <iostream>
#include <string>

int main() {
    const int maxLength = 1 << 16;
    std::string s;
    size_t lastCapacity = s.capacity();

    for (int i = 0; i < maxLength; ++i) {
        s += ' ';
        if (s.capacity() > lastCapacity) {
            std::cout << "capacity: " << s.capacity()
                << std::endl;
            lastCapacity = s.capacity();
        }
    }
    return 0;
}
