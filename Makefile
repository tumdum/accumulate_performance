all: main
	./main

main: main.cpp
	g++ -Wall -Wextra -pedantic -ansi -Os main.cpp -lboost_system -lboost_chrono -o main

append_performance: append_performance.cpp
	g++ -O2 append_performance.cpp -o append_performance
	./append_performance

clean:
	rm -f main
	rm -f append_performance
