// Copyright 2014 Tomasz Kłak (tomasz@tomaszklak.pl)

#include <boost/chrono.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/random.hpp>
#include <boost/bind.hpp>
#include <utility>
#include <algorithm>
#include <string>
#include <vector>

namespace bc = boost::chrono;
namespace br = boost::random;

template <typename F>
bc::nanoseconds run(F& f, const typename F::input_data& data) {
    bc::nanoseconds cumulativeTime;
    const int iterations = 10;
    for (int i = 0; i < iterations; ++i) {
        bc::high_resolution_clock::time_point start =
            bc::high_resolution_clock::now();
        f(data);
        cumulativeTime += bc::high_resolution_clock::now() - start;
    }
    return cumulativeTime / iterations;
}

template <typename Gen, typename F>
typename F::input_data generateInputData(Gen gen, F f) {
    uint64_t size = 1;
    bc::nanoseconds timeTaken;
    typename Gen::input_data input;
    do {
        input = gen(size);
        timeTaken = run(f, input);
        size *= 2;
    } while (timeTaken < bc::milliseconds(500));
    return input;
}

struct CompareResults {
    typedef std::pair<std::string, bc::nanoseconds> element_type;

    explicit CompareResults(const uint64_t problemSize)
        : problemSize_(problemSize) {}
    void add(const std::string& name, const bc::nanoseconds t) {
        results_.push_back(std::make_pair(name, t));
    }
 private:
    friend class CompareResultsPrinter;
    std::vector<element_type> results_;
    const uint64_t problemSize_;
};

struct CompareResultsPrinter {
    typedef CompareResults::element_type element_type;
    explicit CompareResultsPrinter(const CompareResults& compareResults)
        : problemSize_(compareResults.problemSize_) {
        assert(!compareResults.results_.empty());
        results_ = compareResults.results_;
        std::sort(results_.begin(), results_.end(), &elemCompare);
        fastest_ = results_.front().second / problemSize_;
    }

    std::ostream& print(std::ostream& os) const {
        std::for_each(results_.begin(), results_.end(),
            boost::bind(&CompareResultsPrinter::printResult, this,
                boost::ref(os), _1));
        return os;
    }

 private:
    static bool elemCompare(const element_type& lhs, const element_type& rhs) {
        return lhs.second < rhs.second;
    }
    void printResult(std::ostream& os, const element_type& el) const {
        const bc::nanoseconds timePerItem = el.second / problemSize_;
        const float toFastestRatio = timePerItem.count() /
            static_cast<float>(fastest_.count());
        os << el.first << ": " << timePerItem
            << " (" << toFastestRatio << " times fastest)\n";
    }

    std::vector<element_type> results_;
    const uint64_t problemSize_;
    bc::nanoseconds fastest_;
};

std::ostream& operator << (std::ostream& os, const CompareResults& results) {
    return CompareResultsPrinter(results).print(os);
}

template <typename F1, typename F2, typename Gen>
CompareResults compare(Gen gen, F1& f1, F2& f2) {
    typename F1::input_data input = generateInputData(gen, f1);
    CompareResults results(input.size());
    results.add(f1.name(), run(f1, input));
    results.add(f2.name(), run(f2, input));
    return results;
}

template <typename Gen, typename F1, typename F2, typename F3, typename F4,
    typename F5>
CompareResults compare(Gen gen, F1& f1, F2& f2, F3& f3, F4& f4, F5& f5) {
    typename F1::input_data input = generateInputData(gen, f1);
    CompareResults results(input.size());
    results.add(f1.name(), run(f1, input));
    results.add(f2.name(), run(f2, input));
    results.add(f3.name(), run(f3, input));
    results.add(f4.name(), run(f4, input));
    results.add(f5.name(), run(f5, input));
    return results;
}

template <typename Gen>
std::string random_string(Gen& generator, int size) {
    br::uniform_int_distribution<> d(0, 'z'-'a');
    std::string r(size, ' ');
    for (int i = 0; i < size; ++i)
        r[i] = static_cast<char>('a' + d(generator));
    return r;
}

template <typename Gen>
boost::optional<int> random_optint(Gen& generator, int min, int max) {
    br::uniform_int_distribution<> booldist(0, 1);
    if (booldist(generator)) {
        br::uniform_int_distribution<> intdist(min, max);
        return boost::optional<int>(intdist(generator));
    }
    return boost::none;
}

template <typename T1, typename T2>
void verify(T1 t1, T2 t2) {
    if (t1 != t2) {
        std::cerr << t1 << "!=" << t2 << std::endl;
    }
}

template <typename F1, typename F2>
void verifySolutions(const F1& f1, const F2& f2) {
    std::cerr << "Comparing \"" << f1.name() << "\" with \"" << f2.name()
        << "\"\n";
    verify(f1.minFirst, f2.minFirst);
    verify(f1.minSecond, f2.minSecond);
    verify(f1.msg, f2.msg);
}

typedef boost::tuple<boost::optional<int>, boost::optional<int>, std::string>
element_type;

struct InputGenerator {
    typedef std::vector<element_type> input_data;

    explicit InputGenerator(const int seed) : generator(seed) {}

    input_data operator() (const uint64_t size) {
        input_data input(size);
        std::generate(input.begin(), input.end(),
                boost::bind(&InputGenerator::generateElement, this));
        return input;
    }
 private:
    element_type generateElement() {
        return element_type(random_optint(generator, 1, 10),
                random_optint(generator, 1, 10), random_string(generator, 10));
    }

    boost::mt19937 generator;
};

struct Original {
    typedef std::vector<element_type> input_data;

    void operator() (const input_data& input) {
        minFirst = 100;
        minSecond = 100;
        msg = "";

        for (input_data::const_iterator i = input.begin(); i != input.end();
            ++i) {
            if (boost::get<0>(*i)) {
                if (*boost::get<0>(*i) < minFirst) {
                    minFirst = *boost::get<0>(*i);
                }
            }
            if (boost::get<1>(*i)) {
                if (*boost::get<1>(*i) < minSecond) {
                    minSecond = *boost::get<1>(*i);
                }
            }
            msg += " " + boost::get<2>(*i);
        }
    }

    std::string name() const {
        return "original aproach";
    }

    int minFirst;
    int minSecond;
    std::string msg;
};

struct OriginalImproved {
    typedef std::vector<element_type> input_data;

    void operator() (const input_data& input) {
        minFirst = 100;
        minSecond = 100;
        msg = "";

        for (input_data::const_iterator i = input.begin(); i != input.end();
            ++i) {
            if (boost::get<0>(*i)) {
                if (*boost::get<0>(*i) < minFirst) {
                    minFirst = *boost::get<0>(*i);
                }
            }
            if (boost::get<1>(*i)) {
                if (*boost::get<1>(*i) < minSecond) {
                    minSecond = *boost::get<1>(*i);
                }
            }
            msg += " ";
            msg += boost::get<2>(*i);
        }
    }

    std::string name() const {
        return "original aproach with improvements";
    }

    int minFirst;
    int minSecond;
    std::string msg;
};

struct NoConcat {
    typedef std::vector<element_type> input_data;
    void operator() (const input_data& input) {
        minFirst = 100;
        minSecond = 100;
        msg = "";

        for (input_data::const_iterator i = input.begin(); i != input.end();
            ++i) {
            if (boost::get<0>(*i)) {
                if (*boost::get<0>(*i) < minFirst) {
                    minFirst = *boost::get<0>(*i);
                }
            }
            if (boost::get<1>(*i)) {
                if (*boost::get<1>(*i) < minSecond) {
                    minSecond = *boost::get<1>(*i);
                }
            }
        }
    }

    std::string name() const {
        return "no string concatenation";
    }

    int minFirst;
    int minSecond;
    std::string msg;
};

namespace naive {

struct Accumulator {
    int first;
    int second;
    std::string ret;
};

Accumulator operator + (const Accumulator& acc, const element_type& el) {
    Accumulator r(acc);
    if (boost::get<0>(el))
        if (*boost::get<0>(el) < r.first)
            r.first = *boost::get<0>(el);
    if (boost::get<1>(el))
        if (*boost::get<1>(el) < r.second)
            r.second = *boost::get<1>(el);
    r.ret += " ";
    r.ret += boost::get<2>(el);
    return r;
}

struct Accumulate {
    typedef std::vector<element_type> input_data;

    void operator() (const input_data& input) {
        Accumulator acc;
        acc.first = 100;
        acc.second = 100;
        Accumulator r = std::accumulate(input.begin(), input.end(), acc);

        minFirst = r.first;
        minSecond = r.second;
        msg = r.ret;
    }

    std::string name() const {
        return "naive accumulate";
    }

    int minFirst;
    int minSecond;
    std::string msg;
};

}  // namespace naive

struct Accumulator {
    Accumulator(const int first, const int second)
        : first(first), second(second) {}

    Accumulator& operator = (const Accumulator& ac) {
        if (this == &ac) {
            return *this;
        }
        first = ac.first;
        second = ac.second;
        ret = ac.ret;
        return *this;
    }

    int first;
    int second;
    std::string ret;
};

Accumulator& merge(Accumulator& acc, const element_type& el) {
    if (boost::get<0>(el))
        if (*boost::get<0>(el) < acc.first)
            acc.first = *boost::get<0>(el);
    if (boost::get<1>(el))
        if (*boost::get<1>(el) < acc.second)
            acc.second = *boost::get<1>(el);
    acc.ret += " ";
    acc.ret += boost::get<2>(el);
    return acc;
}

struct Accumulate {
    typedef std::vector<element_type> input_data;

    void operator() (const input_data& input) {
        Accumulator acc = std::accumulate(input.begin(), input.end(),
            Accumulator(100, 100), &merge);
        minFirst = acc.first;
        minSecond = acc.second;
        msg = acc.ret;
    }

    std::string name() const {
        return "accumulate";
    }

    int minFirst;
    int minSecond;
    std::string msg;
};

int main() {
    InputGenerator inputGen(std::time(0));
    Original original;
    OriginalImproved originalImproved;
    NoConcat noConcat;
    naive::Accumulate naiveAccumulate;
    Accumulate accumulate;
    std::cout << compare(inputGen, naiveAccumulate, original, noConcat,
        originalImproved, accumulate);
    verifySolutions(naiveAccumulate, original);

    // by definition noConcat will have different msg (empty)
    // verifySolutions(original, noConcat);
    // verifySolutions(noConcat, originalImproved);

    verifySolutions(original, originalImproved);
    verifySolutions(originalImproved, accumulate);
}
